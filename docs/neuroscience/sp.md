
## 1. Semantic Pointer

**Semantic Pointer Hypothesis:** Higher-level cognitive functions in biological systems are made possible by semantic pointers. Semantic pointers are neural representations that carry partial semantic content and are composable into the representational structures necessary to support complex cognition.

Semantic pointers can be well described as vectors in high-dimentional spaces. It is employed because there is too much information related to any particular conceptual representation to be able to actively represent and manipulate in all at the same time.

Semantic pointers are:

- *Mathematically:* vectors in high-dimensional spaces;
- *Physically:* occurrent activity in a biological neural network;
- *Functionally:* compressed representations that point to fuller semantic content.

**Note:** semantic pointers are lossy compressions.

## 2. Semantic Pointers in Nengo

Nengo中，一个具体的ensemble用于表示一个具有不同维度、取值范围的变量。从变量到neuron之间的映射，同过NEF框架完成。NEF的基本原则如下：

1. Neural representations are defined by the combination of nonlinear encoding (exemplified by neuron tuning curves and neural spiking) and weighted linear decoding (over populations of neurons and over time).
2. Transformations of neural representations are functions of the variables represented by neural populations. Transformations are determined using an alternately weighted linear decoding.
3. Neural dynamics are characterized by considering neural representations as state variables of dynamic systems. Thus, the dynamics of neurobiological systems can be analyzed using control (or dynamics systems) theory.

由于Nengo中，从表示角度考虑，运算单元为Tensor之间的四则运算、一维卷积等数学操作，因此，仅从高层次计算描述角度，可以使用Pytorch/Tensorflow等Tensor运算框架实现Nengo的高层功能，其具体底层NEF实现可以等需要实现spiking时再进一步考虑。
