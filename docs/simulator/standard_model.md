## 1. 整体架构

Standard model 整体架构如图所示：

![STANDARD-ARCH](../img/simulator/standard_model.png)

具体来说，Standard model可以分解为四个主要部分：

- Structure and processing

- Memory and Content

- Learning

- Perception and motor

从组件角度来看，Standard model主要包含四种组件：

- Perception and motor

- Working memory

- Declarative long-term memory

- Procedural long-term memory

## 2. 核心流程：Congnitive Cycle

The heart of the standard model is the cognitive cycle：

1. Procedural memory induces the processing required to select a single deliberate act per cycle. Each action can perform multiple modifications to working memory.

2. Changes to working memory can correspond to a step in abstract reasoning or the internal simulation of an external action, but they can also initiate the retrieval of knowledge from longterm declarative memory, initiate motor actions in an external environment, or provide top-down influence to perception. 

3. Complex behavior, both external and internal, arises from sequences of such cycles. In mapping to human behavior, cognitive cycles operate at roughly 50 ms, corresponding to the deliberate-act level in Newell’s hierarchy, although the activities that they trigger can take significantly longer to execute.

## 3. 架构基本假设

| Standard Model架构假设 (Table 1)                                                                                                                                                                         |
|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| &emsp;1. The purpose of architectural processing is to support bounded rationality, not optimality                                                                                                   |
| &emsp;2. Processing is based on a small number of task-independent modules                                                                                                                           |
| &emsp;3. There is significant parallelism in architectural processing                                                                                                                                |
| &emsp;&emsp;a. Processing is parallel across modules                                                                                                                                                 |
| &emsp;&emsp;&emsp;i. ACT-R & Soar: asynchronous; Sigma: synchronous                                                                                                                                  |
| &emsp;&emsp;b. Processing is parallel within modules                                                                                                                                                 |
| &emsp;&emsp;&emsp;i. ACT-R: rule match, Sigma: graph solution, Soar: rule firings                                                                                                                    |
| &emsp;4. Behavior is driven by sequential action selection via a cognitive cycle that runs at ~50 ms per cycle in human cognition                                                                    |
| &emsp;5. Complex behavior arises from a sequence of independent cognitive cycles that operate in their local context, without a separate architectural module for global optimization (or planning). |
| B. Memory and Content                                                                                                                                                                                |
| &emsp;1. Declarative and procedural long-term memories contain symbol structures and associated quantitative metadata                                                                                |
| &emsp;&emsp;a. ACT-R: chunks with activations and rules with utilities; Sigma: predicates and conditionals with functions; Soar: triples with activations and rules with utilities                   |
| &emsp;2. Global communication is provided by a short-term working memory across all cognitive, perceptual, and motor modules                                                                         |
| &emsp;3. Global control is provided by procedural long-term memory                                                                                                                                   |
| &emsp;&emsp;a. Composed of rule-like conditions and actions                                                                                                                                          |
| &emsp;&emsp;b. Exerts control by altering contents of working memory                                                                                                                                 |
| &emsp;4. Factual knowledge is provided by declarative long-term memory                                                                                                                               |
| &emsp;&emsp;a. ACT-R: single declarative memory; Sigma: unifies with procedural memory; Soar: semantic and episodic memories                                                                         |
| C. Learning                                                                                                                                                                                          |
| &emsp;1. All forms of long-term memory content, whether symbol structures or quantitative metadata, are learnable                                                                                    |
| &emsp;2. Learning occurs online and incrementally, as a side effect of performance and is often based on an inversion of the flow of information from performance                                    |
| &emsp;3. Procedural learning involves at least reinforcement learning and procedural composition                                                                                                     |
| &emsp;&emsp;a. Reinforcement learning yields weights over action selection                                                                                                                           |
| &emsp;&emsp;b. Procedural composition yields behavioral automatization                                                                                                                               |
| &emsp;&emsp;&emsp;i. ACT-R: rule composition; Sigma: under development; Soar: chunking                                                                                                               |
| &emsp;4. Declarative learning involves the acquisition of facts and tuning of their metadata                                                                                                         |
| &emsp;5. More complex forms of learning involve combinations of the fixed set of simpler forms of learning                                                                                           |
| D. Perception and Motor                                                                                                                                                                              |
| &emsp;1. Perception yields symbol structures with associated metadata in specific working memory buffers                                                                                             |
| &emsp;&emsp;a. There can be many different such perception modules, each with input from a different modality and its own buffer                                                                     |
| &emsp;&emsp;b. Perceptual learning acquires new patterns and tunes existing ones                                                                                                                     |
| &emsp;&emsp;c. An attentional bottleneck constrains the amount of information that becomes available in working memory                                                                               |
| &emsp;&emsp;d. Perception can be influenced by top-down information provided from working memory                                                                                                     |
| &emsp;2. Motor control converts symbolic relational structures in its buffers into external actions                                                                                                  |
| &emsp;&emsp;a. As with perception, there can be multiple such motor modules                                                                                                                          |
| &emsp;&emsp;b. Motor learning acquires new action patterns and tunes existing ones                                                                                                                   |

## 4. 模型分析

针对Standard model的基本假设，对Soar、ACT-R和Sigma的逐条分析如下：

![对多种模型的分析](../img/simulator/model_analysis.png)

## 参考文档

<!---

Standard Model具体内容参考[PPT](https://gitlab.com/bicf/bicf.gitlab.io/tree/master/docs/attachments/simulator).
-->


Standard model paper: [PAPER](../attachments/simulator/StandardModel.pdf).

Standard Model具体内容参考: [PPT](../attachments/simulator/StandardModel.pptx).


