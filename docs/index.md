disqus:

# 欢迎访问BICF研究组

![logo](./../img/logo-name.jpg "title")

## 实验室简介

脑科学是当今科学领域最重要的、高度交叉的学科，已经远远不再局限于传统生命科学和医学的范畴。清华大学脑与智能实验室将聚集国际一流学者，开展具有开创性、颠覆性、前瞻性的研究，同时带动清华大学工科和生物及医学方向的交叉研究，推动及引领学校的交叉学科发展。实验室的主要研究方向包括开发新型的脑活动测量和调控等下一代关键技术，运用工程技术和计算模型等手段探索脑科学中复杂的前沿科学问题和解决脑疾病及脑健康领域的核心技术问题，攻关类脑技术、推动通用人工智能系统研究等。实验室将尤其注重和国内、国际各领域同行交流，为脑与智能交叉领域的发展做出贡献。

## 团队负责人简介

宋森

2010-Present Principal Investigator, Medical School, Tsinghua University

2004-2010 Postdoctoral Fellow, Department of Brain and Cognitive Sciences, Massachussets Institute of Technology, US

2002-2004, Postdoctoral Fellow, Cold Spring Harbor Laboratory, US

2001-2002, Computational Biologist, GPC Biotech, US

1996-2002, PhD candidate, Department of Biology, Brandeis University, US

1994-1996, B.A., University of Mississippi, US


## 任务分组

### Simulator组

**负责人：渠鹏**

**成员：庞浩、张文博、沈新科、费翔

### Decision Making/Emotion/RL - Basal Ganglion，Hippocampus模型组

**负责人：沈新科**

**成员：庞浩、杜凯、刘锦、张晨皓、李路凯**

*Topic：*

* meta-RL：庞浩
* fMRI/emotion/computational psychiatry：沈新科


### Hippocampus Graphical Model模型组

**模型组负责人：刘祥根**

**成员：张文博、郑浩、孙昱昊、闫宇坤**

*Topic：*

* Time：闫宇坤、郑浩、张文博
* Hierarchy Hyberbolic Semantics：张文博
* Graphical Model/Capsule Network/Rule Learning：刘祥根、孙昱昊、李鹏勇

### Learning (cortex) 孙昱昊组

**负责人：孙昱昊**

**成员：唐李锴、 杜凯**

*Topic：*

* How to do backprop in spiking network：唐李锴、杜凯

### 文献调研监督：沈新科

### 工作任务监督：庞浩

## 领域分组

### 神经科学

**负责人：孙昱昊**

**成员：渠鹏、杜凯**

### 人工智能

**负责人：庞浩**

**成员：刘祥根**

### 认知科学

**负责人：沈新科**

**成员：郑浩、刘锦、张文博**

!!! note "注意"
    **每两周每组组长在组会后汇报一次**




