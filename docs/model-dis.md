disqus:

# 模型讨论（每周三下午）

## 讨论安排

4月17日：讨论Features

### 1、Model





### 2、Simulator

Frank model compare with Cannonical model and Nengo

How to incorporate meta-RL


## 阅读安排

### 2019.4.20添加

* [A Neurobiologically Constrained Cortex Model of Semantic Grounding With Spiking Neurons and Brain-Like Connectivity](./attachments/paper-list/10.3389@fncom.2018.00088.pdf "frontiers in Computational Neuroscience") - 待定
* [Neural representation of word categories is distinct in the temporal lobe: An activation likelihood analysis](./attachments/paper-list/10.1002@hbm.24334.pdf "Hum Brain Mapp") - 待定


## 参与人员：

**渠鹏、庞浩、沈新科、孙昱昊、刘祥根**
