## 孙昱昊负责维护

###主要想法：
。。。

###相关文献：


#### function of recurrent and feedback connection in vision
- Neural dynamics at successive stages of the ventral visual stream are consistent with hierarchical error signals

-
	

#### relevant task
-  vision
-  rule learning chemistry
-  IQ test raven matrix

#### model of local cortical circuit
- Cortical circuits implement optimal context integration
- Visual physiology of the layer 4 cortical circuit in silico
- A Computational Analysis of the Function of Three Inhibitory Cell Types in Contextual Visual Processing

#### 相关实验
-
-

#### capsule network
-


#### Continuous Local Learning
- Synaptic Plasticity Dynamics for Deep Continuous Local Learning
- Theories of Error Back-Propagation in the Brain 
- SuperSpike: Supervised Learning in Multilayer Spiking Neural Networks 
- Surrogate Gradient Learning in Spiking Neural Networks 

