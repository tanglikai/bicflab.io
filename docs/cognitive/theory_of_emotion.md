
# **The quartet theory of human emotions**
==============================================================

For details, see the [attachment powerpoint](./../attachments/cognitive/Koelsch2015_quartet_theory_ppt.pdf).

==============================================================


# **Historical views**
==============================================================

## **Appraisal theory of emotion**

**Core argument: Emotion results from interpretation and explanation of their circumstances even in the absence of physiological arousal.**

The main controversy surrounding these theories argues that emotions cannot happen without physiological arousal.

Magda Arnold: intuitive appraisal - emotions that are good or bad for the person lead to an action.

Richard Lazarus:  as you are experiencing an event, your thought must precede the arousal and emotion (which happen simultaneously).

###**Structural model:**

**Primary appraisal**

Is the situation relevant to my goals? -> the intensity of emotion

Is the situation congruent or incongruent with my goals? -> Different emotions

**Sencondary appraisal**

Who should be accountable for the situation?

Coping potential: problem-focused (change the situation) or emotion-focused (the situation cannot be changed), future expectancy (expectation of changing the situation)

example:  anger - If a person appraises a situation as motivationally relevant, motivationally incongruent, and also holds a person other than himself accountable, the individual would most likely experience anger in response to the situation.

###**Process model** (It is less focused on):

Explain the dynamic nature of emotion. (coping and reappraisal) But it is criticized for it cannot explain the rapid or automatic nature of emotion.

Two-process model of appraisal: perceptual stimuli, associative processing and reasoning (Parallel) 

Multi-level sequential check model: innate, learned and conceptual

Roseman's theory: motive consistency and accountability are two most important components to elicit emotion. Their intensity (certainty and strength) can influence the emotion.

###**Contemporary opinions** (from wikipedia): 

emotion is not just appraisal (consistency with goals, accountability) but a complex multifaceted experience with the following components:

1. Subjective feelings. The appraisal is accompanied by feelings that are good or bad, pleasant or unpleasant (valence), calm or aroused (arousal).

2. Physiological arousal. We can have arousal without emotion, but we cannot have an emotion without arousal. 

3. Expressive behaviors. Emotion is communicated through facial and bodily expressions, postural and voice changes.

4. Action tendencies. Emotions carry behavioral intentions, and the readiness to act in certain ways.

###**评价**

感觉appraisal theory这套理论更多地是解释情绪是怎么产生的，而不是情绪本身的属性。

这套理论最重要的是强调了认知在情绪产生中的作用。但是现代理论认为physiological arousal在情绪中也起着重要作用。


====================================================================

====================================================================

## **The constructive perspective of emotion**

（主要是张丹老师组会胡鑫师姐的报告记录。）

在情绪理论中有一个由来已久的Natural kinds和Constructive perspective的争论。

Natural kinds理论认为人自然地、天生地有几种离散的基本情绪，最著名的是Ekman的六种基本情绪理论。

Constructive perspective在前期主要是批判Natural kinds理论。Natural kinds理论并不能很好地解释周围生理和中枢神经信号。著名的有Schachter & Singer (1962)的情绪认知理论。认为情绪产生决定于两个主要因素：生理唤醒和认知因素。

Natural kinds阵营也提出了Causal appraisal approach (Roseman, 2011)。这两派的争论集中在认知评价独立于情绪还是认知评价是情绪的一部分；在时间上，是认知评价先于情绪还是认知评价与情绪持续互动。即这两派都认同认知过程评价对情绪的重要作用，两派间的壁垒已经较为模糊。

现代心理建构主义认为情绪是人类对于内部和外部模糊信号的主观分类。人的情绪感知包括核心情感（valence, arousal）和对情绪的概念化。（这里可能表述不太准确）

建构主义也开始关注语言对于情绪构建的作用。研究发现语义感知受损的个体对基本的情绪效价分类没有问题，但对于同一效价的不同情绪分类会有问题。2-5岁的儿童也表现出同样的现象(Russell, 2008)。

