disqus:

# 文献阅读列表

## 分享安排


###十月主题 Learning and Local Circuit
Neural dynamics at successive stages of the ventral visual stream are consistent with hierarchical error signals、Reversible Inactivation of Different Millimeter-Scale Regions of Primate IT Results in Different Patterns of Core Object Recognition Deficits和 Hyperbolic Attention Networks
Meta-Learning




###九月主题 Neural Symbolic/Graphical





###七月主题：Decision/Reinforcement Learning





###六月主题：Learning and Local Circuit

* 6月25日，Circuit 
	1. Cortical circuits implement optimal context integration 
	2. Neural dynamics at successive stages of the ventral visual stream are consistent with hierarchical error signals
* 6月18日，Learning 
	1. Synaptic Plasticity Dynamics for Deep Continuous Local Learning；
	2. Meta-Learning paper
* 6月11日，Coding
	1. Exploring Randomly Wired Neural Networks for Image Recognition 
	2. A large-scale, standardized physiological survey reveals higher order coding throughout the mouse visual cortex; 
	3. Hyperbolic Attention Networks
* 6月4日，Deep Learning: A Critical Appraisal

###五月主题：Neural Symbolic/Graphical

* 5月28日，张文博/沈新科 semantics: 
	1. A unified model of human semantic knowledge and its disorders; 
	2. Modality-independent encoding of individual concepts in the left parietal cortex; 
	3. The neuro-cognitive representations of symbols: the case of concrete words
* 5月21日，庞浩/沈新科/渠鹏 rule learning: 
	1. Foundations of human reasoning in the prefrontal cortex
	2. Neural Computations Underlying Causal Structure Learning
	3. Task representations in neural networks trained to perform many cognitive tasks
* 5月14日，郑浩/唐李凯讲海马&learning相关的文献 
	1. The Dance of the Interneurons: How Inhibition Facilitates Fast Compressible and Reversible Learning in Hippocampus;
	2. SuperSpike: Supervised Learning in Multilayer Spiking Neural Networks 
	3. Synaptic Plasticity Dynamics for Deep Continuous Local Learning；
* 5月7日，张文博/李朋勇/刘祥根/闫宇坤讲Relational inductive biases, deep learning, and graph networks

###四月主题：强化学习与决策

* 4月30日，孙昱昊/郑浩，讲Reinforcement learning in artificial and biological systems和A distributed, hierarchical and recurrent framework for reward-based choice
* 4月23日，张文博/庞浩讲Evidence accumulation in a Laplace domain decision space和Integrating Models of Interval Timing and Reinforcement Learning
* 4月16日，刘祥根/庞浩/郑浩讲Wang XJ的前额叶皮层Meta-RL模型
* 4月9日，沈新科/孙昱昊讲Michael J Frank的几个模型
    1. general review of cannonical model
    2. decision making: prefrontal cortex, fMRI  
    3. rule learning: EEG
* 4月2日，庞浩讲Jane Wang的Meta-RL论文

## 阅读安排

### 2019.5.8添加

* [Efficient coding of subjective value](./attachments/paper-list/10.1038@s41593-018-0292-0.pdf "nature neuroscience") - 待定
* [Tracking the flow of hippocampal computation: Pattern separation, pattern completion, and attractor dynamics](./attachments/paper-list/10.1016@j.nlm.2015.10.008.pdf "Neurobiology of Learning and Memory") - 待定
* [Diversity in neural firing dynamics supports both rigid and learned hippocampal sequences](./attachments/paper-list/10.1126@science.aad1935.pdf "science") - 待定

### 2019.5.6添加

* [Multi-Scale Dense Networks for Resource Efficient Image Classification](./attachments/paper-list/1703.09844.pdf "arxiv") - 待定
* [Squeeze-and-Excitation Networks](./attachments/paper-list/1709.01507.pdf "arxiv") - 待定
* [Macroscale cortical organization and a default-like apex transmodal network in the marmoset monkey](./attachments/paper-list/10.1038@s41467-019-09812-8.pdf "nature communications") - 待定

### 2019.5.5添加

* [Self-Attention Capsule Networks for Image Classification](./attachments/paper-list/1904.12483.pdf "arxiv") - 待定
* [Neural dynamics at successive stages of the ventral visual stream are consistent with hierarchical error signals](./attachments/paper-list/10.7554@eLife.42870.001.pdf "eLIFE") - 待定

### 2019.5.4添加

* [Reinforcement Learning, Fast and Slow](./attachments/paper-list/10.1016@j.tics.2019.02.006.pdf "Trends in Cognitive Sciences") - 待定
* [On the interaction of social affect and cognition: empathy, compassion and theory of mind](./attachments/paper-list/10.1016@j.cobeha.2017.07.010.pdf "Behavioral Sciences") - 待定

### 2019.5.2添加

* [Neural Logic Machines](./attachments/paper-list/1904.11694.pdf "arxiv") - 待定

### 2019.4.26添加

* [The number sense is an emergent property of a deep convolutional neural network trained for object recognition](./attachments/paper-list/609347.full.pdf "bioRxiv") - 待定

### 2019.4.24添加

* [Theories of Error Back-Propagation in the Brain](./attachments/paper-list/10.1016@j.tics.2018.12.005.pdf "Trends in Cognitive Sciences") - 待定

### 2019.4.23添加

* [Visual physiology of the layer 4 cortical circuit in silico](./attachments/paper-list/10.1371@journal.pcbi.1006535.pdf "PLOS Computational Biology") - 待定
* [The subiculum: Unique hippocampal hub and more](./attachments/paper-list/10.1016@j.neures.2018.08.002.pdf "Neuroscience Research") - 待定
* [BioNet: A Python interface to NEURON for modeling large-scale networks](./attachments/paper-list/10.1371@journal.pone.0201630.pdf "PLOS ONE") - 待定
* [A large-scale, standardized physiological survey reveals higher order coding throughout the mouse visual cortex](./attachments/paper-list/10.1101@359513.pdf "bioRxiv") - 待定
* [A Computational Analysis of the Function of Three Inhibitory Cell Types in Contextual Visual Processing](./attachments/paper-list/10.3389@fncom.2017.00028.pdf "frontiers in Computational Neuroscience") - 待定
* [A Spiking Neurocomputational Model of High-Frequency Oscillatory Brain Responses to Words and Pseudowords](./attachments/paper-list/10.3389@fncom.2016.00145.pdf "frontiers in Computational Neuroscience") - 待定
* [Cortical circuits implement optimal context integration](./attachments/paper-list/158360.full.pdf "bioRxiv") - 待定
* [Hippocampal Reactivation of Random Trajectories Resembling Brownian Diffusion](./attachments/paper-list/10.1016@j.neuron.2019.01.052.pdf "Neuron") - 待定

### 2019.4.20添加

* [The new genetics of intelligence](./attachments/paper-list/10.1038@nrg.2017.104.pdf "Nature Reviews Genetics") - 待定
* [A unified model of human semantic knowledge and its disorders](./attachments/paper-list/10.1038@s41562-016-0039.pdf "Nature Human Behaviour") - 待定
* [Brain connections of words, perceptions and actions: A neurobiological model of spatiotemporal semantic activation in the human cortex](./attachments/paper-list/10.1016@j.neuropsychologia.2016.07.004.pdf "Neuropsychologia") - 待定
* [Conceptual grounding of language in action and perception: a neurocomputational model of the emergence of category specificity and semantic hubs](./attachments/paper-list/10.1111@ejn.13145.pdf "European Journal of Neuroscience") - 待定
* [Exploring Randomly Wired Neural Networks for Image Recognition](./attachments/paper-list/1904.01569.pdf "arxiv") - 待定
* [GLoMo: Unsupervisedly Learned Relational Graphs as Transferable Representations](./attachments/paper-list/1806.05662.pdf "arxiv") - 待定
* [Functional alignment with anatomical networks is associated with cognitive flexibility](./attachments/paper-list/10.1038@s41562-017-0260-9.pdf "Nature Human Behaviour") - 沈新科/庞浩
* [SuperSpike: Supervised Learning in Multilayer Spiking Neural Networks](./attachments/paper-list/10.1162@neco_a_01086.pdf "Neural Computation") - 待定
* [Surrogate Gradient Learning in Spiking Neural Networks](./attachments/paper-list/1901.09948.pdf "arxiv") - 待定
* [Synaptic Plasticity Dynamics for Deep Continuous Local Learning](./attachments/paper-list/1811.10766.pdf "arxiv") - 待定

### 2019.4.17添加

* [Task representations in neural networks trained to perform many cognitive tasks](./attachments/paper-list/10.1038@s41593-018-0310-2.pdf "nature neuroscience") - 孙昱昊/刘祥根

### 2019.4.9添加

* [The Dance of the Interneurons: How Inhibition Facilitates Fast Compressible and Reversible Learning in Hippocampus](./attachments/paper-list/dance of interneurons.pdf "bioRxiv") - 郑浩

### 2019.4.8添加

* [Deep Learning: A Critical Appraisal](./attachments/paper-list/1801.00631.pdf "arxiv") - 待定
* [Foundations of human reasoning in the prefrontal cortex](./attachments/paper-list/10.1126@science.1252254.pdf "Science") - 沈新科

### 2019.4.7添加

* [Frontal Cortex and the Hierarchical Control of Behavior](./attachments/paper-list/10.1016@j.tics.2017.11.005.pdf "Trends in Cognitive Sciences") - 待定

### 2019.4.6添加

* [Neural dynamics at successive stages of the ventral visual stream are consistent with hierarchical error signals](./attachments/paper-list/elife-42870.pdf "eLIFE") - 孙昱昊
* [Reversible Inactivation of Different Millimeter-Scale Regions of Primate IT Results in Different Patterns of Core Object Recognition Deficits](./attachments/paper-list/10.1016@j.neuron.2019.02.001.pdf "Neuron") - 孙昱昊
* [Hyperbolic Attention Networks](./attachments/paper-list/1805.09786.pdf "arxiv") - 庞浩

### 2019.4.3添加

* [The contribution of the human posterior parietal cortex to episodic memory](./attachments/paper-list/10.1038-nrn.2017.6.pdf "Neuron Perspective") - 孙昱昊
* [Economic Choice as an Untangling of Options into Actions](./attachments/paper-list/j.neuron.2018.06.038.pdf "Neuron Perspective") - 待定
* [Neural dynamics and circuit mechanisms of decision-making](./attachments/paper-list/j.conb.2012.08.006.pdf "Neurobiology") - 孙昱昊
* [Mechanisms of hierarchical reinforcement learning in corticostriatal circuits 1: computational analysis](./attachments/paper-list/bhr114.pdf "Open") - 沈新科
* [A distributed, hierarchical and recurrent framework for reward-based choice](./attachments/paper-list/nrn.2017.7.pdf "nature neuraoscience") - 孙昱昊
* [The contribution of the human posterior parietal cortex to episodic memory](./attachments/paper-list/nrn.2017.6.pdf "nature neuraoscience") - 待定
* [Neural Computations Underlying Causal Structure Learning](./attachments/paper-list/7143.full.pdf "The Journal of Neuroscience") - 沈新科/张文博
* [Integrating Models of Interval Timing and Reinforcement Learning](./attachments/paper-list/j.tics.2018.08.004.pdf "Trends in Cognitive Sciences") - 张文博

### 2019.4.1添加

* [Relational inductive biases, deep learning, and graph networks](./attachments/paper-list/1806.01261.pdf "arxiv") - 待定

### 2019.3.31添加

* [Locomotion-dependent remapping of distributed cortical networks](./attachments/paper-list/10.1038@s41593-019-0357-8.pdf "nature neuroscience") - 待定
* [Reinforcement learning in artificial and biological systems](./attachments/paper-list/s42256-019-0025-4.pdf "nature machine intelligence") - 庞浩
* [Organizing principles of pulvino-cortical functional coupling in humans](./attachments/paper-list/s41467-018-07725-6.pdf "nature communications") - 沈新科
* [Modality-independent encoding of individual concepts in the left parietal cortex](./attachments/paper-list/j.neuropsychologia.2017.05.001.pdf "Neuropsychologia") - 张文博
* [The neuro-cognitive representations of symbols: the case of concrete words](./attachments/paper-list/j.neuropsychologia.2017.06.026.pdf "Neuropsychologia") - 张文博

